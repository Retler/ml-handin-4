import sklearn.datasets
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import time
iris = sklearn.datasets.load_iris()
X = iris['data'][:,0:2] # reduce dimensions so we can plot what happens.
k = 3
print(X.shape)

import numpy as np

def lloyds_algorithm(X, k, T):
    """ Clusters the data of X into k clusters using T iterations of Lloyd's algorithm.

        Parameters
        ----------
        X : Data matrix of shape (n, d)
        k : Number of clusters.
        T : Maximum number of iterations to run Lloyd's algorithm.

        Returns
        -------
        clustering: A vector of shape (n, ) where the i'th entry holds the cluster of X[i].
        centroids:  The centroids/average points of each cluster.
        cost:       The cost of the clustering
    """
    n, d = X.shape

    # Initialize clusters random.
    clustering = np.random.randint(0, k, (n,))
    centroids = np.zeros((k, d))

    # Used to stop if cost isn't improving (decreasing)
    cost = 0
    oldcost = 0

    # Column names
    print("Iterations\tCost")

    for i in range(T):

        # Update centroid

        # YOUR CODE HERE
        for c in range(k):
            relevantXs = [x for i,x in enumerate(X) if clustering[i] == c]  # Get only the x's that are assigned to this centroid
            centroids[c] = sum(relevantXs)/len(relevantXs)
            print("Clustering: ", clustering)
            print("Centroids: ", centroids)
        # END CODE

        # Update clustering

        # YOUR CODE HERE
        distancesOfXsFromClusters = np.zeros((n, k))  # Distance from coordinate x to cluster k_i
        for i in range(k):
            distancesOfXsFromClusters[:,i] = np.linalg.norm(X - centroids[i], axis=1)
        clustering = np.argmin(distancesOfXsFromClusters, axis=1)  # Updating clusterings to be the minimum result along the first axis
        # END CODE


        # Compute and print cost
        cost = 0
        for j in range(n):
            cost += np.linalg.norm(X[j] - centroids[clustering[j]]) ** 2
        print(i + 1, "\t\t", cost)

        # Stop if cost didn't improve more than epislon (decrease)
        if np.isclose(cost, oldcost): break  # TODO
        oldcost = cost

    return clustering, centroids, cost


clustering, centroids, cost = lloyds_algorithm(X, 3, 100)

kmeans = KMeans(n_clusters=3, random_state=0).fit(X)
print("kmeans centers: ", kmeans.cluster_centers_)
print("my centers: ", centroids)